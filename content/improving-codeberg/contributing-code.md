---
eleventyNavigation:
  key: ContributingCode
  title: Contributing Code
  parent: ImprovingCodeberg
---

Hey there, thank you for heading here and your interest in improving Codeberg. This page is intended to give you a quick start on how and where to contribute code to the platform of Codeberg itself. Some things are always changing, and we're not (yet) good at continuously updating the docs on that — but we're always glad to help you with your questions. Just reach out and ask!

Since Codeberg uses [Gitea](https://gitea.io/) as a base, most features that go beyond configuration changes are required upstream and every helping hand is very welcome to improve Gitea and thus Codeberg.

Gitea itself is written in [Go](https://en.wikipedia.org/wiki/Go_(programming_language)), so it's best to make yourself familiar with this language first. It's focused on being a very easy and maintainable language, so it should not be a big issue if you haven't heard of it yet.

Also check out the [Codeberg](https://codeberg.org/Codeberg) and [Codeberg-Infrastructure](https://codeberg.org/Codeberg-Infrastructure) organizations, they contain numerous other software and setup projects that will appreciate your contributions.

## Where do patches go?

We currently sort patches on the following criteria:
- Changes in deployments and config goes to the [Codeberg-Infrastructure/build-deploy-gitea](https://codeberg.org/Codeberg-Infrastructure/build-deploy-gitea) repo. It holds the scripts used to set up the Codeberg instance and overrides other changes, so have a look at it in any case!
- Changes that benefit the general Gitea user base should go into the [upstream repo](https://github.com/go-gitea/gitea). Contributions there also help improve Codeberg, so consider raising a merge request there to help all instances of Gitea
- A small set of patches are committed directly into the [Codeberg/gitea](https://codeberg.org/Codeberg/gitea) repo. These include:
  - (Non-configurable) template changes or small tweaks that are not intended for upstream
  - Hot-fixes for urgent issues (like anti-spam measures) that are sometimes low quality and thus not upstream, but would require a proper solution in the software itself
  - Backports of Gitea commits with important features or fixes that won't be officially backported
  - Cherry-picked commits of pending upstream PRs we want to include sooner.

## Getting started

So let's put our hands-on and start hacking. If you don't already have an issue in mind you want to fix, have a look at the [Community issues](https://codeberg.org/Codeberg/Community/issues) and especially those that are labelled with "[contribution welcome](https://codeberg.org/Codeberg/Community/issues?q=&type=all&state=open&labels=105&milestone=0&assignee=0)", this indicates we would really appreciate your help there 🤗. If you didn't find something yet, go for the Gitea issue tracker. It's likely that you find something interesting amongst the thousands of open reports and feature requests there, especially with the good first issues.

### Working with the `build-deploy-gitea` repo

This is the deployment system for the Codeberg infrastructure, it updates, builds, configures and deploys Gitea.
If you want to work on new features, please don't use it, but choose our [Gitea fork](https://codeberg.org/Codeberg/gitea) instead. You shouldn't normally need to bother running `build-deploy-gitea`.

The deployment of Codeberg is handled via the [Codeberg/build-deploy-gitea](https://codeberg.org/Codeberg/build-deploy-gitea) repository. While there's some work on different deployment approaches (more to that later), this is the way stuff is handled as of today.
The deployment Makefile connects to a remote server via ssh, so we recommend configuring a virtual machine for this. You can use the provided script or perform the following tasks inside your favourite VM / container solution:
- Add a user and group git
- Provide SSH to root
- Set up a MySQL database `gitea` end enter the credentials into your app.ini
- Install at least `git make nodejs npm openssh-server librsvg2-bin` (example package names from Debian Buster)
- Run the Makefile and see that Gitea is installed, you can override variables and run it like `make HOSTNAME_FQDN=localhost deployment`
- After Gitea is installed, you might want to create an admin user for testing (should otherwise be first user by default), you can use `sudo -u git GITEA_WORK_DIR=/data/git GITEA_CUSTOM/etc/gitea /data/git/bin/gitea admin create-user --username you --admin --password yourpassword --email youremail@example.com`
Please see the deployment repo for the folder structure as well as the [upstream docs](https://docs.gitea.io/en-us/) on how to configure and hack on Gitea.

Also note that the Makefile will compile Codeberg's Gitea fork from the `codeberg-test` branch by default. You can override this behaviour by specifying `ORIGIN` and `GITEA_BRANCH` environment to the Makefile.

### Working with the Gitea codebase

Please look at [the Gitea docs](https://docs.gitea.io/en-us/hacking-on-gitea/) on how to start hacking on Gitea itself. They provide more detailed and especially more up-to-date information than we could maintain in our own docs. Additionally, there's probably no need for redundancy here 😉.

### Raising an MR to our Gitea fork

If you fixed an issue specific to Codeberg that should not go into the upstream repo or was refused there, but fits the criteria for our own repo written above, you can file an MR to [our own fork of Gitea](https://codeberg.org/Codeberg/gitea). Make sure to point your MR to the `codeberg` branch and rebase to the latest commits there. Also explain why this should go into the Codeberg repo, if not obvious.

> **Warning** Please note, that the branches may be force-pushed at 
> certain times to make sure the Codeberg commits are well visible and 
> maintainable at any time. This may lead to confusing behaviour of the 
> merge requests, i.e. they seem to include commits you haven't made.
> 
> If you face this issue, you can `git rebase` your commits onto the 
> current Codeberg branch and force-push. 
> ```shell
> git rebase origin/codeberg
> git push -f
> ```

### Working on better deployment methods

If you want to help us improve our deployment scripts, check out the [Codeberg-Infrastructure organization](https://codeberg.org/Codeberg-Infrastructure). We'd really appreciate moving away from Makefile and shell scripts and ask you to join the discussion there.
If you are interested in supporting this project, feel free to contact us. Some legacy resources or WIP might be still invisible to normal users (although we always try to work publicly when possible).
Please make sure to first understand what the `build-deploy-gitea` repo does to replicate its function. Also feel free to contact us (for example via Matrix) to ask where to help.

## More questions?

If you need help or feel stuck, need inspiration or advice, go ahead and ask the community. If you picked up an issue somewhere, feel free to ask your question there. Also, you can always ask in the Matrix channel at [#codeberg@matrix.org](https://matrix.to/#/#codeberg.org:matrix.org).

Thank you very much for your interest in improving Codeberg.
