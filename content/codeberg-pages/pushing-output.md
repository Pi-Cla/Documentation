---
eleventyNavigation:
  key: PushingOutput
  title: Pushing output from SSGs into Codeberg Pages
  parent: CodebergPages
  author: Fayçal Alami Hassani - https://codeberg.org/ka2in
  date: January 2022
  order: 101
---

If you are using a static site generator (SSG) and are satisfied with the result of your project on your local development environment, you can push the files to your Codeberg Pages repository. 

To begin with, we will have two separate repositories, both locally and on Codeberg:

- A main repository for the source files, i.e. where the source files related to your main project will be located. We will refer to this repository as the `source` repository. This repository is the one associated with your [mydocs](/codeberg-pages/examples/docs-as-code/#mydocs) folder. 

- A second repository for Codeberg pages that we will call the `pages` repository. This repository will only contain the files available in the `html` folder located under docs/build/html.

> **Note:**  
> For the purpose of this guide, we have chosen to use two separate folders/repositories. However, you may want to have a different setup that involves creating a [submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules) instead of having folders and repositories in separate locations. With a submodule, your configuration may look like so:
> 
> ```
> ├── sources --> This is a git repo
> │   ├── build
> │   │   └── html
> │   │       └── pages --> This is a separate git repo
> │   └── content
> ```

To copy the content of the `html` folder to your new `pages` folder, run the command:

```bash
(.venv) $ cp -R docs/build/html/ pages 
```

We will now initialize an empty repository inside the pages folder. To do this, type the command:

```bash
$ git init
```

To check the status of your project’s files, type the command: 

```bash
$ git status
```

To add all the files that are available in your directory to Git, run the command:

```bash
$ git add -A
```

Now we will commit:

```bash
$ git commit -m "Example commit message"
```

Repeat these steps in your `source` folder, then push your local commits to the remote repository with the command:

```bash
$ git push origin HEAD:your-remote-branch-name
``` 

> **Note:**  
> Replace `your-remote-branch-name` with the name of your remote branch. It is recommended to initially push your commits to a branch other than the default branch. Once you have made sure everything went smoothly, you can then make a pull request to merge it into the default branch. To learn more about pull requests, read the article [Pull requests and Git flow](https://docs.codeberg.org/collaborating/pull-requests-and-git-flow/).

You should now be able to access your content by visiting https://{username}.codeberg.page. You can find more information in the [Codeberg Pages](/codeberg-pages/) section.
