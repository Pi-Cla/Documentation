---
eleventyNavigation:
  key: CI
  title: Working with Codeberg's CI
  icon: arrows-spin
  order: 65
---


Every piece of code should be tested regularly. Ideally developers already implement unit-tests to test the functionality of code sections.
Some projects even implement a suite of integration tests, testing whether the code in different parts of the software works as a whole and (still) provides the functionality the software promises to deliver.
Running these tests regularly (or continuously) is the job of a Continuous Integration solution.
The results of the tests are displayed to the project members and maintainers enabling them to identify problems and react if errors occur.

## Using Codeberg's instance of Woodpecker CI 

Codeberg currently provides an instance of [Woodpecker CI](https://woodpecker-ci.org/) to some projects.
The service is currently in the closed testing phase. Meaning the access is currently limited to selected persons.

The [README.md](https://codeberg.org/Codeberg-CI/request-access) of the Codeberg-CI repository contains more details in case
you want to apply for access to the Woodpecker CI.

If you are just curious about Woodpecker or already got access to a Woodpecker instance, the Woodpecker project has a [great documentation](https://woodpecker-ci.org/docs/intro) on how to use Woodpecker in your repositories.
