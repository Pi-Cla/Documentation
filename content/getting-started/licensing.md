---
eleventyNavigation:
  key: LicensingOnCodeberg
  title: Licensing on Codeberg
  parent: GettingStarted
  order: 70
---

Adding a licence to a project is essential for making it reusable: In the absence of other agreements, in fact, copyright law would otherwise forbid others from (re-)using and distributing your work. 
Moreover, a licence can further specify other important properties, such as the temporary- or forever-openness of the code, the rights granted to the users (such as the [four fundamental software freedoms](https://www.gnu.org/philosophy/free-sw.html#four-freedoms)), or the permission of using patents encoded in a program.

## Copyright: The mechanism behind most software licences

The international validity of most common licences is guaranteed by copyright law, and in particular by the [Copyright Treaty](https://en.wikipedia.org/wiki/WIPO_Copyright_Treaty) of the United Nations' (UN) World Intellectual Property Organization (WIPO) adopted in 1996. This treaty sets a common denominator for all adhering countries, but individual countries can still extend domestically the reach of copyright. The article 4 of the Copyright Treaty clarified once for all that computer programs are protected by copyright just as literary works. Unfortunately, however, copyright law does not protect hardware objects.

[Copyright law is extremely powerful](https://www.gnu.org/philosophy/enforcing-gpl.en.html). In fact, it grants the author of a work exclusive rights to use or distribute their work. An author can grant permission of using his/her work under arbitrary conditions; just for illustration, an author could invent for example a "coffee licence" and impose that whoever uses his work has to offer the author a coffee. Similarly, forever-open licences grant users the right to use the work provided that they will redistribute any derivative work using the same licence again (copyleft).

## Simplified decision diagram for choosing a software licence

If you start a new software project, the diagram below can help you quickly decide on which free software licence to choose.
If you need a licence for a [non-software project](#hardware-projects), you can skip this section. Later sections will explain the different choices in more details.

### Licence decision diagram

Do you either want to allow people to create proprietary (closed-source) projects with your code, or do you expect your project to remain small (e.g. less than 300 lines)?

* No --> Do you want to allow people to create a closed-source service, for example by using your code on a web server without releasing the source code?
    * No --> We recommend using the **AGPL-3.0-or-later** licence
    * Yes --> Do you want to allow people to use your code as a library and not disclose the source-code of their main program?
        * No --> we recommend using the **GPL-3.0-or-later** licence
        * Yes --> we recommend using the **LGPL-3.0-or-later** licence
* Yes --> Do you want to be able to sue users of your code for [patent infringement](#patent-usage) implemented in the code?
    * No --> We recommend using the **Apache-2.0** licence
    * Yes --> We recommend using the **MIT** licence

## Correctly applying a licence

In order to correctly license your work, you have to explicitly tell which files of your work are under which licence, and how to find the full licence text.
We recommend to follow these steps to avoid ambiguities:

* Important: Make a clear statement about the licence of the project. It is considered sufficient to add the full licence text in a dedicated file in the root directory. Often this file is called 'LICENCE', 'LICENSE' or 'COPYING'. [REUSE](https://reuse.software) suggests to create a 'LICENSES' folder which holds all licence texts. This is also well suited when multiple licences are involved.
* Highly recommended: Add copyright and licence information to each source file.
* Recommended but not necessary: Follow the [tutorial for becoming "REUSE-compliant"](https://reuse.software/tutorial/).

## Major open-source licence categories

### Copyleft vs. permissive

One of the major distinctions between licences is between:

1. Licences which guarantee the perpetual openness of the code, i.e. forever-open licences. This is commonly obtained by a mechanism called "copyleft" which requires redistributing any derivative work using the same licence again. The GPL licences belong to this category. Other mechanisms to achieve the perpetual openness of the code include contractual law, but that is rarely used for software licences. 

2. Licences which permit to close the source, i.e. temporarily-open licences. These licences are usually known as permissive licences. The MIT licence or the Apache licence belong to this category. The term "permissive" often causes confusion, because it sounds fair and good to unexperienced people. 
People who contribute to the development of a program released with a permissive licence must be aware that the program could become proprietary at any time. For example, when a company hires the original team of developers.

If all copyright holders agree, a project can always be forked into a project with a different licence. Therefore, the larger the number of developers, the more difficult it is to make such a change. 

There is no general rule for choosing one type of licence or the other in every situation, but we encourage using copyleft licences whenever possible.

### Free vs. non-free licences

Free software licences are licences which respect the [four fundamental software freedoms](https://www.gnu.org/philosophy/free-sw.en.html#four-freedoms) defined by the Free Software Foundation. 
Both copyleft and permissive licence can, or cannot, be free licences. For example, the [Modified BSD licence](https://www.gnu.org/licenses/license-list.en.html#ModifiedBSD) is a permissive non-copyleft free software licence.

In the context of licences, the term "free" means free as in "freedom", not of gratis, but this has often been confused. Still, free software is often also gratis software. 
Gratis non-free software usually includes gratis proprietary programs (shareware), demonstration or trial versions, limited versions (crippleware), advertising-supported software (e.g. antiviruses), and usually viruses and worms (the victim doesn't pay to get them).

### Patent usage

Some permissive/temporarily-open licences like the MIT licence do not contain a patent provision granting the users the right to use their patents. For example, the code might be licenced under the MIT licence, but implements an algorithm which is protected by a patent. In this case, the licence might allow to copy, modify and distribute the code but at the same time does not guarantee the right to use the patented algorithm. It is a common argument in favour of the MIT licence to claim that no public lawsuit has ever been conducted yet. Still, the threat to be sued remains, and it can be used to exert pressure. Some, if not most, licence disputes moreover are settled even [before reaching the court](https://www.gnu.org/philosophy/enforcing-gpl.en.html) and could therefore leave no trace. Even Google [avoided the use of the MIT licence when developing Android](https://source.android.com/setup/start/licenses), presumably because of the missing patent provision.

For further information on "patents and free software", head over to [this wikipedia article](https://en.wikipedia.org/wiki/Software_patents_and_free_software).

## Warnings

### Licence proliferation

Over the years, hundreds of different software/artwork licences have been written, used and abandoned. This causes a big harm to the Free and Open Source community because it makes it more difficult for everybody to understand their meaning and compatibility between them. A common reaction of developers is to refuse discussions about licences or their use altogether. Rather than dispersing efforts in a multitude of isolated and incompatible efforts ([licence proliferation](https://en.wikipedia.org/wiki/License_proliferation)), we recommend using whenever possible only the mainstream licences as highlighted in this page.

### Licence incompatibilities

It can occur that different projects, licenced with different open-source licences, cannot be used together in a single, often larger, project because the licences require incompatible conditions.
Some licences, in fact, impose on developers (thanks to the power granted by copyright law) some restrictions/obligations on any [derivative work](https://en.wikipedia.org/wiki/Derivative_work). The term "derivative work" has a precise meaning in copyright law: For example, when developers modify the source code written by somebody else, they have created a derivative work. 
As an example, if program A is licenced under GPL version 3 and program B is licenced under the Microsoft Public Licence ([MS-PL](https://directory.fsf.org/wiki/License:MS-PL)), the GPL requires to re-licence any derivative work based on both programs A and B again under GPL, while the MS-PL requires to re-licence the derivative work under MS-PL: the two conditions are incompatible with each other.
The incompatibility between licences is clearly a headache for every developer. A possible way out, and the one we recommend, is to use only mainstream licences (as recommended in this page) and hence avoid [licence proliferation](#licence-proliferation).
For more information on licence compatibility, we recommend the commented [licence list curated by the GNU project](https://www.gnu.org/licenses/license-list.html).

### Conflict-of-interests

The open-source community is represented by a variety of groups with different, and sometimes opposite, interests. For example, the prominent websites [ChooseALicense.com](https://choosealicense.com) is curated by GitHub and can be assumed to reflect at least partially the interests of Microsoft; this website presents, for example GPL last on the front page and it is silent about the missing patent clause in the MIT licence both in the front page and in the [dedicated page](https://choosealicense.com/licenses/mit/ ). Moreover, the wording "I want it simple and permissive" can induce people to favour the MIT licence; laypeople will want to have legal matters simple, and "permissive" sounds like fair and good.
When reading articles on licences (including this one), we recommend exercising discretion by inspecting the governance of the source and the risk of conflicting interests.

### Contributor licence agreement (CLA)

A [contributor licence agreement](https://en.wikipedia.org/wiki/Contributor_License_Agreement) may be forced upon a developer (e.g. by an employer), and may contain clauses which effectively void the spirit of certain open-source licences. For example, a single company may advertise a project (for example for marketing reasons) under a GPL licence, but on the other hand could require all its contributors to assign their copyright to the company. In this way the company could at any time (by being the only copyright holder) change the licence from GPL to a proprietary licence.

## Recommendations for software, hardware and other projects
 
### Software projects

For software projects, we recommend whenever possible to utilize a copyleft licence. The only well-established licences of this type are those of the GNU General Public Licence (GPL) family. The GPL licences have evolved over time and come with different versions. Unfortunately, some versions are not compatible with older ones. To avoid [licence proliferation](#licence-proliferation) and to ease compatibility with future versions, we recommend using the "GNU Affero General Public Licence v3.0 or later" with SPDX licence identifier ["AGPL-3.0-or-later"](https://spdx.org/licenses/). The AGPL provides the maximum openness of the code: Even when the software runs on a server, users who interact with that software (e.g. over a network connection) are entitled to receive a copy of the source code. 
The [Affero GPL v3.0](https://www.gnu.org/licenses/agpl-3.0.en.html) is compatible with the [GPL v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html) and vice-versa thanks to article 13 in each licence. 

If using AGPL-3.0-or-later is not an option, we recommend using another licence of the GNU family, such as the General Public Licence (GPL), or the Lesser General Public Licence (LGPL), always indicating the version "**v3.0 or later**". 
In some cases, such as when writing certain libraries, using the Lesser General Public Licence (LGPL) may lead to a broader usage. In such cases, the [LGPL is preferred over the (A)GPL](https://www.gnu.org/licenses/gpl-faq.en.html#WhySomeGPLAndNotLGPL).
For software which is not very long (e.g. less than 300 lines of code) or unimportant, rather than using a copyleft licence you may consider using a [lax permissive licence instead](https://www.gnu.org/licenses/gpl-faq.en.html#WhatIfWorkIsShort). 

If you are not allowed to, or if you do not want to use a forever-open licence (for example because the company you work for does not allow you to do so) we recommend using a temporarily-open or permissive (in the sense that it permits users to close source the code) licence which contains a patent provision. To avoid [proliferation of licences](#licence-proliferation), we recommend therefore to use the Apache licence version 2.0 (SPDX licence identifier Apache-2.0).

We recommend following the guidelines defined by the [GNU project](https://www.gnu.org/licenses/licenses.html). They are recommended by the [Free Software Foundation](https://fsf.org), too.

### Hardware projects

We distinguish primarily between two families of hardware projects: 

1. Hardware projects with little or no software dependencies (e.g. projects containing CAD drawings)
2. Hardware projects containing important software dependencies (e.g. the design of an integrated circuit) where compatibility issues with other licences may arise

In the first case, we recommend the use of the CERN Open Hardware Licence Version 2 - Strongly Reciprocal (SPDX licence identifier: CERN-OHL-S-2.0). This licence guarantees the forever-openness of the project as well as the actual hardware objects, but is incompatible with other mainstream licences like those of the GNU-GPL family. 

In the second case, there is currently no copyleft/forever-open licence with wide licence compatibility which is able to protect a hardware device. This is primarily due to the fact that hardware objects are not protected by copyright law. While awaiting for the appearance of a suited licence, we recommend the use the "GNU Affero General Public Licence v3.0 or later" whenever the use of a CERN Open Hardware Licence is not permitted.

For hardware projects which should allow creating proprietary forks and/or proprietary hardware objects a licence can be used as described in the next section.

### Other projects (documentation, music, etc.)

For documentation, writing, and other non-code assets, a [Creative Commons (CC)](https://creativecommons.org/about/cclicenses/) licence can be used, but note that only the following CC licences are considered free (sorted from protective to unconditional):

* [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/)
* [CC-BY](https://creativecommons.org/licenses/by/4.0/)
* [CC0](https://creativecommons.org/publicdomain/zero/1.0/)

Like CC themselves, Codeberg **recommends against** using a [CC licence on code](https://creativecommons.org/faq/#can-i-apply-a-creative-commons-license-to-software).

### FAQ

We do not curate our own FAQ. Instead we redirect to other major collections:

* [Frequently Asked Questions about the GNU Licences](https://www.gnu.org/licenses/gpl-faq.en.html)
* [Contributor's Frequently Asked Questions (FAQ) guide](https://www.fsf.org/licensing/contributor-faq)
* [Licensing and Compliance Lab: The most frequently asked Frequently Asked Questions](https://www.fsf.org/blogs/licensing/licensing-and-compliance-lab-the-most-frequently-asked-frequently-asked-questions)

If you have further questions on licensing, feel free to reach out to our community Matrix channels. Access links are provided at the [contact page](/contact/).

### Spelling: licence or license?

* In the USA: licen**s**e = noun, to licen**s**e = verb
* Everywhere else: licen**c**e = noun, to licen**s**e = verb

Since Codeberg is located in Germany, the latter is used.
